## Generate token

Generate token and login from following route by passing email-id and password.
```
/login
```
## Create user

To create user use following route with generated authentication token using POST method.
```
/user 
```
## List all user

To list all the users use following route with generated authentication token using GET method.

```
/user
```
## Update user
To update user use following route with generated authentication token using UPDATE method.

```
/user/:id/update
```

## Delete user
To delete user use following route with generated authentication token using DELETE method (applicable only if logged In user is admin).

```
/user/:id/delete
```
## Check parenthesis

Checks whether parenthesis entered by user have correct order. if input contains correct order entry of the user will be created with a count as many times network call is made   
```
/balance
```