const checkToken = require('../../middleware/index')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');
const matchString = require('./checkParenthesis');
module.exports = function (app, db) {
    app.post('/user', async (req, res) => {
        const data = await db.user.create({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            role: req.body.role,
            email_id: req.body.email,
            password: bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10)),
        })
        return res.status(200).send(data)
    });

    app.get('/user', checkToken, async (req, res) => {
        const userData = await db.user.findOne({ where: { email_id: req.decoded.email_id } })
        if (userData.dataValues.role === 'admin') {
            const userData = await db.user.findAll()
            return res.status(200).send({
                userData
            })
        }
        else {
            return res.status(401).send({
                message: 'Invalid access'
            })
        }

    })

    app.patch('/user/:id/update', checkToken, async (req, res) => {
        await db.user.update({ first_name: req.body.first_name }, { where: { id: req.params.id } })
        return res.status(200).send({
            message: 'updated successfully'
        })
    })

    app.delete('/user/:id/delete', checkToken, async (req, res) => {
        const userData = await db.user.findOne({ where: { email_id: req.decoded.email_id } })
        if (userData.dataValues.role === 'admin') {
            await db.user.destroy({ where: { id: req.params.id } })
            return res.status(200).send({
                message: 'deleted successfully'
            })
        }
        else {
            return res.status(401).send({
                message: 'Invalid access'
            })
        }

    })

    //login
    app.post('/authenticate', async (req, res) => {
        const userData = await db.user.findOne({ where: { email_id: req.body.email } })
        if (userData === null) {
            return res.status(401).json({
                message: "authentication failed"
            })
        }
        bcrypt.compare(req.body.password, userData.dataValues.password, (err, result) => {
            if (err) {
                return res.status(401).json({
                    message: "Something went wrong"
                })
            }
            if (result) {
                let token = jwt.sign({ email_id: req.body.email }, config.secret, { expiresIn: '24h' })
                return res.status(200).json({
                    message: "successfully logged in",
                    token: token
                })
            }
            else {
                return res.status(401).json({
                    message: "authentication failed"
                })
            }
        })

    })


    app.post('/balance', checkToken, async (req, res) => {
        const userData = await db.user.findOne({ where: { email_id: req.decoded.email_id } })
        const balancedata = await db.check.findOne({ where: { email_id: req.decoded.email_id } })
        const a = matchString(req.body.input)
        if (a) {
            if (balancedata) {
                await db.check.update({ attempts: balancedata.dataValues.attempts + 1 }, { where: { email_id: req.decoded.email_id } })
                return res.status(200).send({
                    username: userData.dataValues.first_name + " " + userData.dataValues.last_name,
                    status: "balanced",
                    attempts: balancedata.dataValues.attempts + 1
                })
            }
            else {
                await db.check.create({
                    name: userData.dataValues.first_name + " " + userData.dataValues.last_name,
                    email_id: userData.dataValues.email_id,
                    attempts: 1
                })
                return res.status(200).send({
                    username: userData.dataValues.first_name + " " + userData.dataValues.last_name,
                    status: "balanced",
                    attempts: 1
                })
            }
        }
        else {
            await db.check.update({ attempts: balancedata.dataValues.attempts + 1 }, { where: { email_id: req.decoded.email_id } })
            return res.status(400).json({
                username: userData.dataValues.first_name + "" + userData.dataValues.last_name,
                status: "Unbalanced",
                attempts: balancedata ? balancedata.dataValues.attempts + 1 : 1
            })
        }
    })
}

