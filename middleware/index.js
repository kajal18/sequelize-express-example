const jwt = require('jsonwebtoken');
const config = require('../config/config')
let checkToken = async (req, res, next) => {
    try {
        let token = req.headers.authorization
        if (token) {
            jwt.verify(token, config.secret, (err, decoded) => {
                if (err) {
                    return res.json({
                        success: false,
                        message: "Failed to authenticate token"
                    })
                }
                req.decoded = decoded;
                next();
            })
        }
        else {
            return res.json({
                success: false,
                message: "Token is needed"
            })
        }
    }
    catch (err) {
        return next(err)
    }
}



module.exports = checkToken 