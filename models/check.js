module.exports = function (sequelize, DataTypes) {
    var Check = sequelize.define('check', {
        name: {
            type: DataTypes.STRING
        },
        email_id: {
            type: DataTypes.STRING
        },
        attempts: {
            type: DataTypes.INTEGER
        }
    }, {
            timestamps: true,
            paranoid: true
        });

    return Check;
};