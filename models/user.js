module.exports = function (sequelize, DataTypes) {
    var User = sequelize.define('user', {
        first_name: {
            type: DataTypes.STRING
        },
        last_name: {
            type: DataTypes.STRING
        },
        role: {
            type: DataTypes.ENUM('user', 'admin'),
            defaultValue: 'user'
        },
        email_id: {
            type: DataTypes.STRING
        },
        password: {
            type: DataTypes.STRING
        }
    }, {
            timestamps: true,
            paranoid: true
        });

    return User;
};