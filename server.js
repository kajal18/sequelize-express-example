const express = require("express");
const bodyParser = require("body-parser");
var db = require('./models');
const route = require('./app/routes/index')
const port = 6002
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

route(app, db)

db.sequelize.sync().then(function () {
    app.listen(port, function () {
        console.log(`Listening on PORT ${port}`);
    });
});